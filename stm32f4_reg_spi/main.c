#include "rcc_config.h"
#include "delay.h"

void spi_config(void)
{
	/*********** STEPS TO FOLLOW ***********
	1. Enable SPI clock
	2. Configure the Control Register 1
	3. Configure the CR2
	****************************************/
	RCC->APB2ENR |= (1<<12); // enable SPI clock
	SPI1->CR1 |= (1<<0) | (1<<1); // CPOL=1. CPHA=1
	SPI1->CR1 |= (1<<2); // master mode
	SPI1->CR1 |= (3<<3); // BR[2:0] = 011: fPCLK/16, PCLK2 = 80MHz, SPI clk = 5MHz
	SPI1->CR1 &= ~(1<<7); // LSB first = 0, MSB first
	SPI1->CR1 |= (1<<8) | (1<<9); // SSM=1, SSI=1 -> Software Slave Management
	SPI1->CR1 &= ~(1<<10); // RXONLY = 0, full-fuplex
	SPI1->CR1 &= ~(1<<11); // DFF=0, 8 bit data
	SPI1->CR2 = 0;
}

void gpio_config (void)
{
	RCC->AHB1ENR |= (1<<0); // 	enable GPIOA clock
	GPIOA->MODER |= (1<<8) | (2<<10) | (2<<12) | (2<<14); // output for PA4 and alternate functions for PA5, PA6, PA7
	GPIOA->OSPEEDR |= (3<<8) | (3<<10) | (3<<12) | (3<<14); // HIGH speed for PA4, PA5, PA6, PA7
	GPIOA->AFR[0] |= (5<<20) | (5<<24) | (5<<28); // AFR(SPI1) for PA5, PA6, PA7
}

void spi_enable (void)
{
	SPI1->CR1 |= (1<<6); // SPE=1, peripheral enabled
}

void spi_disable (void)
{
	SPI1->CR1 &= ~(1<<6); // SPE=0, peripheral disbled
}

void cs_enable (void)
{
	GPIOA->BSRR |= (1<<4)<<16;
}

void cs_disable (void)
{
	GPIOA->BSRR |= (1<<4);
}

void spi_transmit (uint8_t	*data, int size)
{
	/*********** STEPS TO FOLLOW ***********
	1. Wait for the TXE bit to set in the Status Register
	2. Write the data to the Data Register
	3. After the data has been transmitted, wait for the BSY bit to reset in Status Register
	4. Clear the Overrun flag by reading DR and SR
	****************************************/
	int i=0;
	while(i<size)
	{
		while(!((SPI1->SR) & (1<<1))); // wait for TXE bit to set -> this will indicate that the buffer is empty
		SPI1->DR = data[i]; // load the data into the Data Register
		i++;			
	}
			
	/*During discontinuous communications, there is a 2 APB clock period delay between the
	write operation to the SPI_DR register and BSY bit setting. As a consequence it is
	mandatory to wait fitst until TXE is set and then until BSY is cleared after writing the last
	data*/
	
	while (!((SPI1->SR) & (1<<1))); // wait for TXE bit to set -> this will indicate that the buffer is empty
	while (((SPI1->SR) & (1<<7))); // wait for BSY bit to reset -> this will indicate that SPI is not bussy in communication
	
	uint8_t temp = SPI1->DR;
					temp = SPI1->SR;
}

uint8_t data[3] = {0x02, 0x32, 0xf6};

int main (void)
{
	sys_clock_config();
	gpio_config();
	cs_disable();
	spi_config();
	
	spi_enable();
		
	cs_enable();
	spi_transmit(data, 3);
	cs_disable();
	
	while(1)
	{
	}
}