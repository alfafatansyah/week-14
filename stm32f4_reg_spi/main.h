#define id_read 0x06	// read
#define id_write 0x05	// write
#define id_cmd 0x04		// command

#define code_sys_dis 0x00
#define code_sys_en 0x01
#define code_led_off 0x02
#define code_led_on 0x03
#define code_blynk_off 0x08
#define code_blynk_on 0x09
#define code_slave_mode 0x10
#define code_master_mode 0x18
#define code_ext_clk_mode 0x1C
#define code_com_nmos_8 0x20
#define code_com_nmos_16 0x24
#define code_com_pmos_8 0x28
#define code_com_pmos_16 0x2C
#define code_pwm_1_16 0xA0
#define code_pwm_2_16 0xA1
#define code_pwm_3_16 0xA2
#define code_pwm_4_16 0xA3
#define code_pwm_5_16 0xA4
#define code_pwm_6_16 0xA5
#define code_pwm_7_16 0xA6
#define code_pwm_8_16 0xA7
#define code_pwm_9_16 0xA8
#define code_pwm_10_16 0xA9
#define code_pwm_11_16 0xAA
#define code_pwm_12_16 0xAB
#define code_pwm_13_16 0xAC
#define code_pwm_14_16 0xAD
#define code_pwm_15_16 0xAE
#define code_pwm_16_16 0xAF