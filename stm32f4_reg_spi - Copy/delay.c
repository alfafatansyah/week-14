#include "delay.h"
#include "rcc_config.h".h"

void tim2_config (void)
{
	/******************* STEPS FOLLOWED ********************	
	1. Enable Timer clock
	2. Set the prescaler and the ARR
	3. Enable the Timer, and wait for the update Flag to set	
	********************************************************/
	
	// 1. Enable Timer clock
	RCC->APB1ENR |= (1<<0); // Enable the timer2 clock
	
	// 2. Set the prescaler and the ARR
	TIM2->PSC = 100-1; // 100MHz/100 = 1MHz -- 1 us delay
	TIM2->ARR = 0xffff-1; // MAX ARR value
	
	// 3. Enable the Timer, and wait for the update Flag to set
	TIM2->CR1 |= (1<<0); // Enable the Counter
	while (!(TIM2->SR & (1<<0))); // UIF: Update interrupt flag.. This bit is set by hardware when registers are update
}

void delay_us (uint16_t us)
{
	/******************* STEPS FOLLOWED ********************	
	1. Reset the Counter
	2. Wait for the Counter to reach the entered value. As each count will take 1 us,
		 the total waiting time will be the required us delay	
	********************************************************/
	TIM2->CNT = 0;
	while(TIM2->CNT < us);
}

void delay_ms (uint16_t ms)
{
	for (uint16_t i=0; i<ms; i++)
	{
		delay_us(1000); // delay of 1 ms
	}
}