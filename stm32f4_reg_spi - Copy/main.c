#include "main.h"
#include "rcc_config.h"
#include "delay.h"

uint16_t data_send;

void spi_config(void)
{
	RCC->APB2ENR |= (1<<12);			// enable SPI clock
	SPI1->CR1 |= (1<<0) | (1<<1);	// CPOL=1. CPHA=1
	SPI1->CR1 |= (1<<2); 					// master mode
	SPI1->CR1 |= (3<<3);					// BR[2:0] = 011: fPCLK/16, PCLK2 = 80MHz, SPI clk = 5MHz
	SPI1->CR1 &= ~(1<<7);					// LSB first = 0, MSB first
	SPI1->CR1 |= (1<<8) | (1<<9);	// SSM=1, SSI=1 -> Software Slave Management
	SPI1->CR1 &= ~(1<<10);				// RXONLY = 0, full-fuplex
	SPI1->CR1 |= (1<<11);					// DFF=1, 16 bit data
	SPI1->CR2 = 0;
}

void gpio_config (void)
{
	RCC->AHB1ENR |= (1<<0);																	// enable GPIOA clock	
	GPIOA->MODER |= (1<<8) | (2<<10) | (2<<12) | (2<<14);		// output for PA4 and alternate functions for PA5, PA6, PA7
	GPIOA->OSPEEDR |= (3<<8) | (3<<10) | (3<<12) | (3<<14);	// HIGH speed for PA4, PA5, PA6, PA7
	GPIOA->AFR[0] |= (5<<20) | (5<<24) | (5<<28);						// AFR(SPI1) for PA5, PA6, PA7
}

void spi_enable (void)
{
	SPI1->CR1 |= (1<<6);	// SPE=1, peripheral enabled
}

void spi_disable (void)
{
	SPI1->CR1 &= ~(1<<6);	// SPE=0, peripheral disbled
}

void cs_enable (void)
{
	GPIOA->BSRR |= (1<<4)<<16;
}

void cs_disable (void)
{
	GPIOA->BSRR |= (1<<4);
}

void spi_transmit (uint16_t	data)
{	
	cs_enable();
	
	while (!((SPI1->SR) & (1<<1)));	
	SPI1->DR = data;	
  while (!((SPI1->SR) & (1<<1)));
  while ((SPI1->SR) & (1<<7));	
	
	cs_disable();
}

void cmd_set (uint8_t cmd_data, uint8_t add_data,  uint8_t code_data)
{
	uint16_t temp;
	switch(cmd_data)
	{
		case id_cmd:
			temp = (cmd_data<<13) | (code_data<<6);
			spi_transmit(temp);
			break;
		case id_write:
			temp = (cmd_data<<13) | (add_data<<6) | (code_data<<2);
			spi_transmit(temp);
			break;
	}
}
	
void ht_1632c_init (void)
{
	cmd_set(id_cmd, 0, code_sys_dis);
	cmd_set(id_cmd, 0, code_com_nmos_16);
	cmd_set(id_cmd, 0, code_master_mode);
	cmd_set(id_cmd, 0, code_sys_en);
	cmd_set(id_cmd, 0, code_led_on);
	for(int i = 0; i <= 0x5F; i++)
	{
		cmd_set(id_write, i, 0x0F);
	}
}

int main (void)
{
	sys_clock_config();
	gpio_config();
	cs_disable();
	spi_config();	
	spi_enable();
	
	ht_1632c_init();
	
	while(1)
	{
	}
}