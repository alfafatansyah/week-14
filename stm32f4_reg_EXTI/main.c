#include "rcc_config.h"

void gpio_config (void)
{
	/********** STEP FOLLOWS **********
	1. Enable GPIO clock
	2. Set the registered Pin in the INPUT mode
	3. Configure the PULL UP / PULL DOWN According to your requirement
	**********************************/
	RCC->AHB1ENR |= (1<<0); // enable GPIOA clock
	GPIOA->MODER &= ~(1<<4); // Bits 5:4 = 0:0 -> PA2 is input
	GPIOA->PUPDR |= (1<<4); // Bits 5:4 = 0:1 -> PA2 is Pull-up mode
}

void interrupt_config (void)
{
	/********** STEP FOLLOWS **********
	1. Enable the SYSCNFG bit in RCC register
	2. Configure the EXTI configuration Register in the SYSCNFG
	3. Enable the EXTI using Interrupt Mask Register (IMR)
	4. Configure the Rising Edge / Falling Edge Trigger
	5. Set the Interrupt Priority
	6. Enable the interrupt
	**********************************/
	
	RCC->APB2ENR |= (1<<14); // Enable 
	SYSCFG->EXTICR[0] &= ~(0Xf<<8); // Bits 11:10:9:8 = 0:0:0:0 -> configure EXTI 2 line for PA2
	EXTI->IMR |= (1<<2); // Disable Mask on EXTI2
	EXTI->RTSR |= (1<<2); // Enable Rising edge Trigger for PA2
	EXTI->FTSR &= ~(1<<2); // Disable Falling edge Trigger for PA2
	NVIC_SetPriority (EXTI2_IRQn, 2);
	NVIC_EnableIRQ (EXTI2_IRQn);
}

int flag = 0;
int count = 0;

void delay (uint32_t time)
{
	while(time--)
	{}
}

void EXTI2_IRQHandler (void)
{
	/********** STEP FOLLOWS **********
	1. Check the Pin, which triggered the Interrupt
	2. Clear the Interrupt Pending Bit
	**********************************/
	
	if(EXTI->PR & (1<<2)) // if the interrupt is triggered by PA2
	{
		EXTI->PR |= (1<<1); // Clear the Interrupt Pending bit
		flag =1;
	}
}

int main (void)
{
	sys_clock_config();
	gpio_config();
	interrupt_config();
	
	while(1)
	{
		if (flag)
		{
			delay (100000);
			count++;
			flag = 0;
		}
	}
}