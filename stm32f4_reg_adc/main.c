#include "rcc_config.h"

void adc_init (void)
{
	/********** STEPS TO FOLLOW **********
	1. Enable ADC and GPIO clock
	2. Set the prescalar in the Common Control Register (CCR)
	3. Set the Scan Mode and Resolution in the Control Register 1 (CR1)
	4. Set the Continuous Conversation, EOC, and Data Aligment in Control Reg 2 (CR2)
	5. Set the Sampling Time for the Channels in ADC_SMPRx
	6. Set the Regular Channel Sequence length in ADC_SQR1
	7. Set the Respective GPIO PINs in the Analog Mode
	**************************************/
	
	// 1. Enable ADC and GPIO clock
	RCC->APB2ENR |= (1<<8); // enable ADC1 clock
	RCC->AHB1ENR |= (1<<0); // enable GPIOA clock
	
	// 2. Set the prescalar in the Common Control Register (CCR)
	ADC->CCR |= (1<<16); // PCLK2 devide by 4
	
	// 3. Set the Scan Mode and Resolution in the Control Register 1 (CR1)
	ADC1->CR1 |= (1<<8); // SCAN mode enabled
	ADC1->CR1 &= ~(1<<24); // 12 bit RES
	
	// 4. Set the Continuous Conversation, EOC, and Data Aligment in Control Reg 2 (CR2)
	ADC1->CR2 |= (1<<1); // enable continuous conversion mode
	ADC1->CR2 |= (1<<10); // EOC after each conbersion
	ADC1->CR2 &= ~(1<<11); // Data Aligment RIGHT
	
	// 5. Set the Sampling Time for the Channels in ADC_SMPRx
	ADC1->SMPR2 &= ~((1<<6) | (1<<9)); // sampling time of 3 cycles for channel 2 and channel 3
	
	// 6. Set the Regular Channel Sequence length in ADC_SQR1
	ADC1->SQR1 |= (1<<20); // SQR1_L =1 for 2 conversions
	
	// 7. Set the Respective GPIO PINs in the Analog Mode
	GPIOA->MODER |= (3<<4); // analog mode for PA 2
	GPIOA->MODER |= (3<<6); // analog mode for PA 3
}

void adc_enable (void)
{
	/********** STEPS TO FOLLOW **********
	1. Enable the ADC by setting ADON bit in CR2
	2. Wait for ADC to stabilize (approx 10us)
	**************************************/
	
	ADC1->CR2 |= 1<<0; // ADON =1 enable ADC1
	
	uint32_t delay = 10000;
	while (delay--);
}

void adc_start (int channel)
{
	/********** STEPS TO FOLLOW **********
	1. Set the channel Sequence in the SQR Register
	2. Clear the Status register
	3. Start the Conversion by Setting the SWSTART bit in CR2
	**************************************/
	
	/* Since we will be polling for each channel, here we will keep one channel in the sequence at a time
	ADC1->SQR3 |= (channel<<0); will just keep the respective channel in the sequence for the conversion*/
	
	ADC1->SQR3 = 0;
	ADC1->SQR3 |= (channel<<0); // conversion in regular sequence
	
	ADC1->SR = 0; // clear the status register
	
	ADC1->CR2 |= (1<<30); // start the conversion	
}

void adc_wait_for_conv (void)
{
	/********** STEPS TO FOLLOW **********
	EOC Flag will be set, once the conversion is finished
	**************************************/
	
	while (!(ADC1->SR & (1<<1))); // wait for EOC flag to set
}

uint16_t adc_get_val (void)
{
	return ADC1->DR; // read the data register
}

void adc_disable (void)
{
	/********** STEPS TO FOLLOW **********
	1. Disable the ADC by Clearing ADON bit in CR2
	**************************************/
	
	ADC1->CR2 &= ~(1<<0); // disable ADC
}

uint16_t adc_val[2] = {0,0};

int main (void)
{
	sys_clock_config();
	adc_init();
	adc_enable();
	
	while(1)
	{
		adc_start(2);
		adc_wait_for_conv();
		adc_val[0] = adc_get_val();
		
		adc_start(3);
		adc_wait_for_conv();
		adc_val[1] = adc_get_val();
	}
}