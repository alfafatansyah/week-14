#include "RccConfig.h"
#include "delay.h"

int z = 0;

void i2c_config (void)
{
	/***** STEPS FOLLOWED *****
	1. Enable THE I2C CLOCK and GPIO CLOCK
	2. Configure the I2C PINs for Alternate Functions
		 a) Select Alternate Function in MODER Register
		 b) Select Open Drain Output
		 c) Select High SPEED for the PINs
		 d) Select Pull-up for both the Pins
		 e) Configure the Alternate Function in AFR Register
	3. Reset the I2C
	4. Program the peripheral input clock in I2C_CR2 Register in order to generate cirrect timings
	5. Configure the clock control registers
	6. Configure the rise time register
	7. Program the I2C_CR1 register to enable the peripheral
	***************************/
	
	// 1. Enable THE I2C CLOCK and GPIO CLOCK
	RCC->AHB1ENR |= (1<<1); // enable GPIOB CLOCK
	RCC->APB1ENR |= (1<<21); // enable I2C CLOCK	
	
	// 2. Configure the I2C PINs for Alternate Functions
	GPIOB->MODER |= (2<<12) | (2<<14); // Bits (13:12) & (15:14) = 1:0 --> Alternate Function for Pin PB6 & PB7
	GPIOB->OTYPER |= (1<<6) | (1<<7); // bit6=1, bit7=1 output open drain
	GPIOB->OSPEEDR |= (3<<12) | (3<<14); // Bit(12:13) & (14:15) = 1:1 --> High Speed for PIN PB6 & PB7
	GPIOB->PUPDR |= (1<<12) | (1<<14); // Bits (12:13) & (14:15) = 0:1 --> Pull up for PIN PB8
	GPIOB->AFR[0] |= (4<<24) | (4<<28); // Bits (24:25:26:27) & (28:29:30:31) = 0:1:0:1
	
	// Configure the I2C PINs for ALternate Functions
	//GPIOB->MODER |= (2<<16) | (2<<18);  // Bits (17:16)= 1:0 --> Alternate Function for Pin PB8; Bits (19:18)= 1:0 --> Alternate Function for Pin PB9
	//GPIOB->OTYPER |= (1<<8) | (1<<9);  //  Bit8=1, Bit9=1  output open drain
	//GPIOB->OSPEEDR |= (3<<16) | (3<<18);  // Bits (17:16)= 1:1 --> High Speed for PIN PB8; Bits (19:18)= 1:1 --> High Speed for PIN PB9
	//GPIOB->PUPDR |= (1<<16) | (1<<18);  // Bits (17:16)= 0:1 --> Pull up for PIN PB8; Bits (19:18)= 0:1 --> pull up for PIN PB9
	//GPIOB->AFR[1] |= (4<<0) | (4<<4);  // Bits (3:2:1:0) = 0:1:0:0 --> AF4 for pin PB8;  Bits (7:6:5:4) = 0:1:0:0 --> AF4 for pin PB9
	
	// 3. Reset the I2C
	I2C1->CR1 |= (1<<15);
	I2C1->CR1 &= ~(1<<15);
	
	// 4. Program the peripheral input clock in I2C_CR2 Register in order to generate cirrect timings
	I2C1->CR2 |= (50<<0); // PCLK1 frequency in MHz
	
	// 5. Configure the clock control registers
	I2C1->CCR = (250<<0); // check calculation in PDF
	
	// 6. Configure the rise time register
	I2C1->TRISE = 51; // check PDF again
	
	// 7. Program the I2C_CR1 register to enable the peripheral
	I2C1->CR1 |= (1<<0); // Enable I2C	
}

void i2c_start (void)
{
	/***** STEPS FOLLOWED *****
	1. Send the START condition
	2. Wait for the SB (Bit 0 in SR1) to set. This incicate that the start condition is generated
	***************************/
	
	I2C1->CR1 |= (1<<10);  // Enable the ACK
	I2C1->CR1 |= (1<<8); // Generate START
	while(!(I2C1->SR1 & (1<<0))); // Wait for SB bit to set
}

void i2c_address (uint8_t address)
{
	/***** STEPS FOLLOWED *****
	1. Send the Slave Address to the DR Register
	2. Wait fot the ADDR (bit 1 in SR1) to set. This indicates the end of address transmission
	3. Clear the ADDR by reading the SR1 and SR2
	***************************/
	
	I2C1->DR = address; // send the address
	while (!((I2C1->SR1) & (1<<1))); // wait for ADDR bit to set
	uint8_t temp = I2C1->SR1 | I2C1->SR2; // read SR1 and SR2 to clear the ADDR bit
}

void i2c_write (uint8_t data)
{
	/***** STEPS FOLLOWED *****
	1. Wait for the TXE (bit 7 in SR1) to set. This indicates that the DR is empty
	2. Send the DATA to the DR Register
	3. Wait for the BTF (bit 2 in SR1) to set. This indicates the end of LAST DATA transmission
	***************************/
	
	while(!(I2C1->SR1 & (1<<7))); // wait for TXE bit to set
	I2C1->DR = data;
	while(!(I2C1->SR1 & (1<<2))); // wait for BTF bit to set
}

void i2c_stop (void)
{
	I2C1->CR1 |= (1<<9); // Stop I2C
}

void i2c_write_multi (uint8_t *data, uint8_t size)
{
	/***** STEPS FOLLOWED *****
	1. Wait for the TXE (bit 7 in SR1) to set. This indicates that the DR is empty
	2. Keep Sending DATA to the DR Register after performing the check if the TXE bit is set
	3. Once the DATA transfer is complete, Wait for the BTF (bit 2 in SR1) to set. This indicates the end of LAST DATA transmission
	***************************/
	
	while(!(I2C1->SR1 & (1<<7))); // wait for TXE bit to set
	while(size)
	{
		while(!(I2C1->SR1 & (1<<7))); // wait for TXE bit to set
		I2C1->DR = (volatile uint32_t) *data++;
		size--;
	}
	while(!(I2C1->SR1 & (1<<2))); // wait for BTF bit to set
}

int main(void)
{
	z++;
	SysClockConfig();
	z++;
	tim2_config();
	z++;
	i2c_config();
	z++;	
	i2c_start();
	z++;	
	i2c_address(0x4E);
	
	z++;
	i2c_address(0xE4);
	z++;
	i2c_write(0x02);
	z++;
	i2c_stop();
	z++;
	while(1)
	{

	}
}