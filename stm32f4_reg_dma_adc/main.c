#include "rcc_config.h"
#include "delay.h"

void adc_init (void)
{
	/********** STEPS TO FOLLOW **********
	1. Enable ADC and GPIO clock
	2. Set the prescalar in the Common Control Register (CCR)
	3. Set the Scan Mode and Resolution in the Control Register 1 (CR1)
	4. Set the Continuous Conversation, EOC, and Data Aligment in Control Reg 2 (CR2)
	5. Set the Sampling Time for the Channels in ADC_SMPRx
	6. Set the Regular Channel Sequence length in ADC_SQR1
	7. Set the Respective GPIO PINs in the Analog Mode
	**************************************/
	
	// 1. Enable ADC and GPIO clock
	RCC->APB2ENR |= (1<<8); // enable ADC1 clock
	RCC->AHB1ENR |= (1<<0); // enable GPIOA clock
	
	// 2. Set the prescalar in the Common Control Register (CCR)
	ADC->CCR |= (1<<16); // PCLK2 devide by 4
	
	// 3. Set the Scan Mode and Resolution in the Control Register 1 (CR1)
	ADC1->CR1 |= (1<<8); // SCAN mode enabled
	ADC1->CR1 &= ~(1<<24); // 12 bit RES
	
	// 4. Set the Continuous Conversation, EOC, and Data Aligment in Control Reg 2 (CR2)
	ADC1->CR2 |= (1<<1); // enable continuous conversion mode
	ADC1->CR2 |= (1<<10); // EOC after each conbersion
	ADC1->CR2 &= ~(1<<11); // Data Aligment RIGHT
	
	// 5. Set the Sampling Time for the Channels in ADC_SMPRx
	ADC1->SMPR2 &= ~((7<<6) | (7<<9)); // sampling time of 3 cycles for channel 2 and channel 3
	
	// 6. Set the Regular Channel Sequence length in ADC_SQR1
	ADC1->SQR1 |= (3<<20); // SQR1_L =3 for 4 conversions
	
	// 7. Set the Respective GPIO PINs in the Analog Mode
	GPIOA->MODER |= (3<<4); // analog mode for PA 2
	GPIOA->MODER |= (3<<6); // analog mode for PA 3

	/********************
	adc_dma_init();
	********************/
	
	// Sampling Freq for Temp Sensor
	ADC1->SMPR1 |= (7<<18); // Sampling time of 20us
	ADC1->SMPR1 |= (7<<24); // Sampling time of 20us
	// Set the TSVREFE Bit to wake the sensor
	ADC->CCR |= (1<<23);
	
	
	ADC1->CR2 |= (1<<8); //	Enable DMA for ADC
	ADC1->CR2 |= (1<<9); // Enable Contiuous Request
	ADC1->SQR3 |= (2<<0); // SEQ1 for Channel 2
	ADC1->SQR3 |= (3<<5); // SEQ2 for Channel 3
	ADC1->SQR3 |= (16<<10); // SEQ3 for Channel 16
	ADC1->SQR3 |= (18<<15); // SEQ4 for Channel 18
}

void adc_enable (void)
{
	/********** STEPS TO FOLLOW **********
	1. Enable the ADC by setting ADON bit in CR2
	2. Wait for ADC to stabilize (approx 10us)
	**************************************/
	
	ADC1->CR2 |= 1<<0; // ADON =1 enable ADC1
	
	uint32_t delay = 10000;
	while (delay--);
}

void adc_start (void)
{
	/********** STEPS TO FOLLOW **********
	1. Clear the Status register
	3. Start the Conversion by Setting the SWSTART bit in CR2
	**************************************/

	ADC1->SR = 0; // clear the status register
	
	ADC1->CR2 |= (1<<30); // start the conversion	
}

void dma_init (void)
{
	// Enable the DMA2 clock
	RCC->AHB1ENR |= (1<<22); // DMA2EN = 1
	// Select the Data Direction
	DMA2_Stream0->CR &= ~(3<<6); // Peripheral to memory
	// Select Circular mode
	DMA2_Stream0->CR |= (1<<8); // CIRC = 1
	// Enable Memory Address Increment
	DMA2_Stream0->CR |= (1<<10); // MIC = 1
	// Set the size for data
	DMA2_Stream0->CR |= (1<<11)|(1<<13); // PSIZE = 01, MSIZE = 01, 16 bit data
	// Select channel for the stream
	DMA2_Stream0->CR &= ~(7<<25); // Channel 0 selected
}

void dma_config (uint32_t src_add, uint32_t dest_add, uint16_t size)
{
	DMA2_Stream0->NDTR |= size; // Set the size of the transfer
	DMA2_Stream0->PAR = src_add; // Source address is peripheral address
	DMA2_Stream0->M0AR = dest_add; // Destination Address is memory address
	// Enable the DMA Stream
	DMA2_Stream0->CR |= (1<<0); // EN =1
}
	
uint16_t rx_data[4];
float tempereature16, tempereature18;

int main (void)
{
	sys_clock_config();
	tim2_config();
	
	adc_init();
	adc_enable();
	dma_init();
	
	dma_config((uint32_t) &ADC1->DR, (uint32_t) rx_data, 4);
	
	adc_start();
	
	while (1)
	{
		tempereature16 = (((float)(3.3*rx_data[2]/(float)4095) - 0.76) / 0.0025) + 25;	
		tempereature18 = (((float)(3.3*rx_data[3]/(float)4095) - 0.76) / 0.0025) + 25;			
	}
}